import uproot
import os
os.environ['TF_ENABLE_ONEDNN_OPTS'] = '0'
import tensorflow as tf
tf.random.set_seed(123)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os.path
import datetime
    

from VAE import read_testing_samples
from VAE import sampling
from VAE import test
from VAE import save_trees

def main():

    vae = tf.keras.models.load_model('vae.keras')
    encoder = tf.keras.models.load_model('encoder.keras')
    decoder = tf.keras.models.load_model('decoder.keras')
    
    # Data original + VAE 
    df = read_testing_samples('data_merged_processed.root:data22')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'data_merged_processed.root:data22')
        
    # W+jets + VAE
    df = read_testing_samples('wjets_merged_processed.root:wjets_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'wjets_merged_processed.root:wjets_NoSys')
    
    # ttbar + VAE
    df = read_testing_samples('ttbar_merged_processed.root:ttbar_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'ttbar_merged_processed.root:ttbar_NoSys')
    
    # Z+jets + VAE
    df = read_testing_samples('zjets_merged_processed.root:zjets_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'zjets_merged_processed.root:zjets_NoSys')

    # singletop + VAE
    df = read_testing_samples('singletop_merged_processed.root:singletop_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'singletop_merged_processed.root:singletop_NoSys')
  
    # triboson + VAE
    df = read_testing_samples('triboson_merged_processed.root:triboson_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'triboson_merged_processed.root:triboson_NoSys')    
    
    # gammajet + VAE
    df = read_testing_samples('gammajet_merged_processed.root:gammajet_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'gammajet_merged_processed.root:gammajet_NoSys')      

    # signal_200_50 + VAE
    df = read_testing_samples('signal_200_50_merged_processed.root:signal_200_50_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'signal_200_50_merged_processed.root:signal_200_50_NoSys')
    
    # signal_800_0 + VAE
    df = read_testing_samples('signal_800_0_merged_processed.root:signal_800_0_NoSys')
    decoded_dataset = test(df,encoder,decoder)
    save_trees(df, decoded_dataset, 'signal_800_0_merged_processed.root:signal_800_0_NoSys')    
    
if __name__ == "__main__":
    main()
